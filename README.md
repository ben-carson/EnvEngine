Envelope Engine
***
Java-based envelope system maintenance application.

Inspired by Dave Ramsey, and fed up with manual data entry on Google spreadsheets, I am attempting to create a computational engine for the envelope system that I use for budgeting.