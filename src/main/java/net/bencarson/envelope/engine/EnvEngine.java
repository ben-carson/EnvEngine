package net.bencarson.envelope.engine;

public class EnvEngine {

	public static int computeTwenties(double amount) {
		int twentiesNeeded = 0;
		twentiesNeeded = (int) (amount / 20);
		double amountRemaining = amount % 20;
		return twentiesNeeded;
	}
	
	public static int computeTens(double amount) {
		int tensNeeded = 0;
		tensNeeded = (int) (amount / 10);
		return tensNeeded;
	}
	
	public static int computeFivers(double amount) {
		int fivesNeeded = 0;
		fivesNeeded = (int) (amount / 5);
		return fivesNeeded;
	}
	
	public static int computeSingles(double amount) {
		int onesNeeded = 0;
		onesNeeded = (int) (amount / 1);
		return onesNeeded;
	}
	
	public static int computeQuarters(double amount) {
		int quartersNeeded = 0;
		quartersNeeded = (int) (amount / 0.25);
		return quartersNeeded;
	}
	
	public static int computeDimes(double amount) {
		int dimesNeeded = 0;
		dimesNeeded = (int) (amount / 0.10);
		return dimesNeeded;
	}
	
	public static int computeNickels(double amount) {
		int nickelsNeeded = 0;
		nickelsNeeded = (int) (amount / .05);
		return nickelsNeeded;
	}
	
	public static int computePennies(double amount) {
		int penniesNeeded = 0;
		penniesNeeded = (int) (amount / .01);
		return penniesNeeded;
	}
}
